package com.life.lifecassette.core.platform.provider

import android.content.Context
import android.content.SharedPreferences
import com.life.lifecassette.utils.extentions.*
import com.google.gson.Gson
import com.google.gson.JsonIOException
import com.google.gson.JsonSyntaxException
import extensions.platform.ISO_8601_FORMAT
import extensions.platform.formatted
import extensions.platform.toDate
import java.io.Serializable
import java.util.*

open class BasePrefsProvider(context: Context, private val gson: Gson) {

    private val preferences: SharedPreferences by lazy {
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    private fun <U> setPreference(name: String, value: U) = with(preferences.edit()) {
        when (value) {
            is Long -> putLong(name, value)
            is String -> putString(name, value)
            is Int -> putInt(name, value)
            is Boolean -> putBoolean(name, value)
            is Float -> putFloat(name, value)
            null -> putInt("", 0)
            else -> throw java.lang.IllegalArgumentException("This type can't be saved to Preferences")
        }.apply()
    }

    @Suppress("UNCHECKED_CAST", "IMPLICIT_CAST_TO_ANY")
    private fun <T> getPreference(name: String, default: T): T = with(preferences) {
        val res = when (default) {
            is Long -> preferences.getLong(name, default)
            is String -> preferences.getString(name, default)
            is Int -> preferences.getInt(name, default)
            is Boolean -> preferences.getBoolean(name, default)
            is Float -> preferences.getFloat(name, default)
            else -> throw java.lang.IllegalArgumentException("This type can't be get from Preferences")
        }
        res as T
    }

    protected fun setDatePreference(name: String, value: Date) {
        setPreference(name, value.formatted(ISO_8601_FORMAT))
    }

    protected fun getDatePreference(name: String, default: Date?): Date? {
        val stringDate = getPreference(name, "")
        return if (stringDate.isBlank()) {
            null
        } else {
            stringDate.toDate(ISO_8601_FORMAT)
        }
    }

    @Throws(JsonIOException::class)
    fun putSerializable(
        key: String, o: Serializable?
    ) = preferences.edit().putString(key, o?.toJson(gson)).apply()

    @Throws(JsonSyntaxException::class)
    fun <T> getSerializable(
        key: String, type: Class<T>
    ): T? where T : Serializable {
        return preferences.getString(key, null)
            ?.to(gson, type)
    }

    fun registerChangeListener(listener: SharedPreferences.OnSharedPreferenceChangeListener) {
        preferences.registerOnSharedPreferenceChangeListener(listener)
    }

    fun unregisterChangeListener(listener: SharedPreferences.OnSharedPreferenceChangeListener) {
        preferences.unregisterOnSharedPreferenceChangeListener(listener)
    }

    fun removeAll() {
        preferences.edit()
            .clear()
            .apply()
    }

    fun remove(name: String) {
        with(preferences.edit()) { remove(name) }.apply()
    }

    companion object {
        const val TAG = "SettingsProvider"
        const val PREFS_NAME = "LashifyPreferences"
    }
}