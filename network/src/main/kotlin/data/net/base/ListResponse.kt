package data.net.base

import com.google.gson.annotations.SerializedName

open class ListResponse<out T> {
    @SerializedName("list") private val _list: List<T>? = emptyList()
    @SerializedName("total_count") var totalCount: Int = 0
    @SerializedName("offset") var offset: Int = 0
    val list: List<T>
        get() = _list ?: emptyList()
}