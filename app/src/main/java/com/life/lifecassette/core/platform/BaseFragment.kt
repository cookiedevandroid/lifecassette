package com.life.lifecassette.core.platform

import android.os.Bundle
import android.view.*
import androidx.navigation.fragment.findNavController
import com.life.lifecassette.utils.extentions.screenslog
import core.exception.Failure
import core.experimental.LiveDataResourceReadyView
import core.platform.BaseCoreFragment
import com.beach_weather.base.extensions.platform.hideAnim
import com.beach_weather.base.extensions.platform.showAnim
import com.life.lifecassette.R

abstract class BaseFragment : BaseCoreFragment(), LiveDataResourceReadyView {
    protected val activity: BaseActivity get() = (requireActivity() as BaseActivity)

    abstract fun initUI(savedInstanceState: Bundle?)
    abstract fun initViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        screenslog(this.javaClass.canonicalName ?: "null")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        findNavController()
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI(savedInstanceState)
        initViewModel()
    }

    override fun handleFailure(failure: Failure) {
        (activity as? LiveDataResourceReadyView)?.handleFailure(failure)
    }

    override fun showProgress(requestId: Int?) {
        activity.findViewById<View>(R.id.progress)?.showAnim()
    }

    override fun hideProgress(requestId: Int?) {
        activity.findViewById<View>(R.id.progress)?.hideAnim()
    }
}