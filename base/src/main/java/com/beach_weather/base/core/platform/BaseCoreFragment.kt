package core.platform

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import extensions.rx.plus
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject

abstract class BaseCoreFragment : Fragment() {
    protected val disposables = CompositeDisposable()
    private val viewCreationPublisher = PublishSubject.create<Boolean>()
    private var viewCreationDisposable: Disposable? = null

    abstract val layoutRes: Int
    open val TAG: String by lazy { simpleName() }

    protected fun simpleName(): String = this.javaClass.simpleName

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = if (layoutRes <= 0) null else inflater.inflate(layoutRes, container, false)

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    fun autoDisposable(disposableFun: () -> Disposable) {
        disposables + disposableFun()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (viewCreationPublisher.hasObservers()) {
            viewCreationPublisher.onNext(true)
        }
    }

    protected fun waitForViewCreation(func: () -> Unit) {
        if (view != null) {
            func.invoke()
        } else {
            viewCreationDisposable = viewCreationPublisher.take(1).subscribe { func.invoke() }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposeViewPublisher()
    }

    private fun disposeViewPublisher() {
        viewCreationDisposable?.dispose()
        viewCreationDisposable = null
    }

    protected fun <T> LiveData<T>.observe(action: (T) -> Unit) {
        this.observe(this@BaseCoreFragment, Observer {
            action(it)
        })
    }
}