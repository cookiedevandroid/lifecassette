package core.experimental

import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import java.util.concurrent.atomic.AtomicBoolean

class SinglePerOwnerLiveEvent<T> : MutableLiveData<T>() {

    private val pendingMap: MutableMap<Int, AtomicBoolean> = mutableMapOf()

    @MainThread
    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {

        super.observe(owner, Observer {
            val hash = owner.lifecycle.hashCode()
            val pending = pendingMap[hash] ?: let {
                pendingMap[hash] = AtomicBoolean(false)
                pendingMap[hash]
            }

            val available = pending?.compareAndSet(true, false) == true
            if (available) {
                observer.onChanged(it)
            }
        })
    }

    @MainThread
    override fun setValue(t: T?) {
        super.setValue(t)
    }

    /**
     * Used for cases where T is Void, to make calls cleaner.
     */
    @MainThread
    fun reset() {
        value = null
    }
}