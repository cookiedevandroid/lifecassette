package com.beach_weather.base.extensions.platform

import android.annotation.SuppressLint
import androidx.navigation.NavController
import java.util.*

@SuppressLint("RestrictedApi")
fun NavController.checkDestination(value: String): Boolean =
    currentDestination?.parent?.displayName?.contains(value.toLowerCase(Locale.getDefault()))
        ?: false

fun NavController.checkGraphCall(label: String): Boolean = graph.label == label