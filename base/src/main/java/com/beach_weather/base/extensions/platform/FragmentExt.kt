package extensions.platform

import android.content.res.Resources
import android.os.Bundle
import android.os.Parcelable
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

/**
 * Created by artCore on 4/9/19.
 */
inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) =
    beginTransaction().func().commit()

fun Fragment.toastShort(text: CharSequence) {
    activity?.let { Toast.makeText(it, text, Toast.LENGTH_SHORT).show() }
}

fun Fragment.toast(text: CharSequence) {
    activity?.let { Toast.makeText(it, text, Toast.LENGTH_LONG).show() }
}

fun Fragment.toast(@StringRes resId: Int) {
    activity?.let { Toast.makeText(it, resId, Toast.LENGTH_LONG).show() }
}

fun Fragment.showOrReplaceLast(tag: String, replaceFunc: FragmentTransaction.() -> Unit) {
    val manager = childFragmentManager
    val fragment = manager.findFragmentByTag(tag)
    if (fragment?.isHidden == true) {
        manager.beginTransaction().show(fragment).commit()
    }

    if (fragment == null || !fragment.isVisible) {
        if (activity?.isFinishing?.not() == true) {
            val transaction = manager.beginTransaction()
            transaction.replaceFunc()
            transaction.commit()
        }
    }
}

inline fun <reified T : Fragment> Fragment.fragmentById(id: Int): T? =
    childFragmentManager.findFragmentById(id) as? T

inline fun <reified T : Fragment> Fragment.fragmentByTag(tag: String): T? =
    childFragmentManager.findFragmentByTag(tag) as? T

fun Fragment.toastNotImplemented() {
    activity?.let {
        Toast.makeText(it, """ not implemented yet ¯\_(ツ)_/¯ """, Toast.LENGTH_SHORT).show()
    }
}

fun <P : Parcelable, F : Fragment> F.withArgs(pair: Pair<String, P>): F {
    return this.apply {
        arguments = Bundle().apply {
            putParcelable(pair.first, pair.second)
        }
    }
}

inline fun <F : Fragment> F.withArgs(func: Bundle.() -> Unit): F {
    return this.apply {
        arguments = Bundle().apply { func() }
    }
}