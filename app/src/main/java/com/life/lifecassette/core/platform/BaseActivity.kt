package com.life.lifecassette.core.platform

import android.os.Bundle
import android.view.*
import androidx.annotation.StringRes
import com.life.lifecassette.domain.failures.validations.EmptyRequiredFieldValidationFailure
import com.beach_weather.base.core.domain.failures.HttpFailure
import com.beach_weather.base.core.domain.failures.NetworkConnectionFailure
import com.beach_weather.base.core.domain.failures.ParseFailure
import com.beach_weather.base.core.domain.failures.UnknownFailure
import com.beach_weather.base.extensions.platform.hideAnim
import com.beach_weather.base.extensions.platform.showAnim
import com.life.lifecassette.R
import core.exception.Failure
import core.experimental.LiveDataResourceReadyView
import core.platform.BaseCoreActivity
import extensions.platform.message
import extensions.platform.okButton
import extensions.platform.showAlert
import extensions.platform.title

abstract class BaseActivity : BaseCoreActivity(), LiveDataResourceReadyView {

    private val unknownErrorMessage by lazy { getString(R.string.error_unknown) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun showProgress(requestId: Int?) {
        findViewById<View>(R.id.progress)?.showAnim()
    }

    override fun hideProgress(requestId: Int?) {
        findViewById<View>(R.id.progress)?.hideAnim()
    }

    override fun handleFailure(failure: Failure) {
        when (failure) {
            is NetworkConnectionFailure -> {
                renderError(
                    R.string.network_connection_error,
                    R.string.network_connection_error_title
                )
            }
            is HttpFailure -> renderError(failure.message ?: getString(R.string.error_http_fail))
            is ParseFailure -> renderError(R.string.error_parse_fail)
            is Failure.ValidationFailure -> handleValidationFailure(failure)
            is UnknownFailure -> renderError(failure.message ?: unknownErrorMessage)
            else -> renderError(R.string.error_unknown)
        }
    }

    private fun handleValidationFailure(failure: Failure.ValidationFailure) {
        val messageResource = when (failure) {
            is EmptyRequiredFieldValidationFailure -> R.string.error_empty_required_field
            else -> R.string.error_unknown
        }
        renderError(messageResource)
    }

    @Suppress("MemberVisibilityCanBePrivate")
    protected fun renderError(errorMessage: String) = showErrorAlert(errorMessage = errorMessage)

    @Suppress("MemberVisibilityCanBePrivate")
    protected fun renderError(@StringRes resource: Int, @StringRes titleRes: Int = R.string.error_title) {
        showErrorAlert(getString(titleRes), getString(resource))
    }

    fun showErrorAlert(title: String =  getString(R.string.error_title), errorMessage: String, function: () -> Unit = {}) {
        showAlert {
            title(title)
            message(errorMessage)
            okButton {function.invoke() }
            setCancelable(true)
        }
    }
}
