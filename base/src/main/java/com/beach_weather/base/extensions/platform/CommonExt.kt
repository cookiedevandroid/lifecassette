@file:JvmName("CommonExt")

package extensions.platform

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import com.google.gson.Gson
import java.io.IOException
import java.nio.charset.Charset

fun hasLollipop() = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
fun hasNougat() = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
fun hasMarshMellow() = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
fun hasKitKat() = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
fun hasOreo() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O

fun hasLollipop(func: () -> Unit) {
    if (hasLollipop()) {
        func()
    }
}

fun hasNougat(func: () -> Unit) {
    if (hasNougat()) {
        func()
    }
}

fun Context.readFromJson(fileName: String): String? {
    val json: String?
    return try {
        val `is` = assets.open("data/$fileName.json")
        val size = `is`.available()
        val buffer = ByteArray(size)
        `is`.read(buffer)
        `is`.close()
        json = String(buffer, Charset.forName("UTF-8"))
        json
    } catch (ex: IOException) {
        ex.printStackTrace()
        null
    }
}

fun doForPreLollipop(func: () -> Unit) {
    if (hasLollipop().not()) {
        func()
    }
}

fun hasKitKat(func: () -> Unit) {
    if (hasKitKat()) {
        func()
    }
}

fun Any.toJson() = Gson().toJson(this)

fun Context.getAppBuildVersion(): String? {
    val name = try {
        val pInfo = packageManager.getPackageInfo(packageName, 0)
        pInfo.versionName
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
        null
    }

    return name
}