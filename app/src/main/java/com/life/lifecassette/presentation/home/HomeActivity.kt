package com.life.lifecassette.presentation.home

import android.os.Bundle
import com.life.lifecassette.core.platform.BaseActivity
import com.life.lifecassette.R

class HomeActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

}