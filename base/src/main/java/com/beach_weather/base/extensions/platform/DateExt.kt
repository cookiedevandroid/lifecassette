package extensions.platform

import android.annotation.SuppressLint
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

const val ISO_8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"
const val ISO_8601_FORMAT_NO_Z = "yyyy-MM-dd'T'HH:mm:ss"
const val TIME_FORMAT = "hh:mm aa"

enum class Lang { FRA, ENG }

fun Date.formatted(
    pattern: String = ISO_8601_FORMAT,
    locale: Locale = Locale.getDefault(),
    timeZone: TimeZone = TimeZone.getDefault()
): String {
    val format = SimpleDateFormat(pattern, locale).apply { this.timeZone = timeZone }
    return format.format(this)
}

fun Date.toCalendar(): Calendar {
    val calendar = Calendar.getInstance()
    calendar.time = this
    return calendar
}

@Throws(ParseException::class)
fun dateFrom(
    string: String,
    pattern: String = ISO_8601_FORMAT,
    locale: Locale = Locale.getDefault(),
    timeZone: TimeZone = TimeZone.getTimeZone("UTC"),
    retryWith: (() -> Date?)? = null
): Date? {
    val format = SimpleDateFormat(pattern, locale)
    format.timeZone = timeZone
    return try {
        format.parse(string)
    } catch (ex: ParseException) {
        return retryWith?.invoke()
    }
}

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@SuppressLint("SimpleDateFormat")
fun timeBetweenInterval(
    openTime: String,
    closeTime: String
): Boolean {
    return try {
        val dateFormat = SimpleDateFormat(TIME_FORMAT)
        val afterCalendar = Calendar.getInstance().apply {
            time = dateFormat.parse(openTime)
            add(Calendar.DATE, 1)
        }
        val beforeCalendar = Calendar.getInstance().apply {
            time = dateFormat.parse(closeTime)
            add(Calendar.DATE, 1)
        }

        val current = Calendar.getInstance().apply {
            val localTime = dateFormat.format(timeInMillis)
            time = dateFormat.parse(localTime)
            add(Calendar.DATE, 1)
        }
        current.time.after(afterCalendar.time) && current.time.before(beforeCalendar.time)
    } catch (e: ParseException) {
        e.printStackTrace()
        false
    }
}

@SuppressLint("SimpleDateFormat")
@Throws(ParseException::class)
fun dateTo(
    string: String,
    pattern: String = ISO_8601_FORMAT,
    toPattern: String,
    locale: Locale = Locale.getDefault(),
    timeZone: TimeZone = TimeZone.getTimeZone("UTC")
): String? {
    val format = SimpleDateFormat(pattern, locale)
    format.timeZone = timeZone
    return try {
        format.parse(string)?.let { date ->
            val output = SimpleDateFormat(toPattern, locale)
            output.timeZone = timeZone
            output.format(date)
        } ?: run {
            return null
        }
    } catch (ex: ParseException) {
        return null
    }
}

fun Date.truncateToDay(timeZone: TimeZone): Date {
    val calendar = Calendar.getInstance(timeZone)
    calendar.time = this
    calendar.set(Calendar.HOUR_OF_DAY, 0)
    calendar.set(Calendar.MINUTE, 0)
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0)
    return calendar.time
}

fun getCalendarAtGmt() = Calendar.getInstance(TimeZone.getTimeZone("GMT"))

fun Date.getDayComparator(timeZone: TimeZone = TimeZone.getDefault()): Comparator<Date> {
    return Comparator { date1, date2 ->
        date1.truncateToDay(timeZone).compareTo(date2.truncateToDay(timeZone))
    }
}

fun Date.dateYear(): Int {
    val calendar = Calendar.getInstance()
    calendar.time = this
    return calendar.get(Calendar.YEAR)
}

fun Calendar.year(): Int {
    return this.get(Calendar.YEAR)
}

fun Calendar.month(): Int {
    return this.get(Calendar.MONTH)
}

fun Calendar.date(): Int {
    return this.get(Calendar.DATE)
}

fun Calendar.hourOfDay(): Int {
    return this.get(Calendar.HOUR_OF_DAY)
}

fun Calendar.minute(): Int {
    return this.get(Calendar.MINUTE)
}

fun Calendar.toDate(useTimeZone: TimeZone = TimeZone.getDefault()): Date {
    return time.apply { timeZone = useTimeZone }
}

fun getUserLocale(lang: Lang?): Locale {
    return when (lang) {
        null -> Locale.getDefault()
        Lang.ENG -> Locale.ENGLISH
        Lang.FRA -> Locale.FRANCE
    }
}