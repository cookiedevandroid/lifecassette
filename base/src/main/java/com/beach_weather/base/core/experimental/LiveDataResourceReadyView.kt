package core.experimental

import androidx.lifecycle.LifecycleOwner
import com.beach_weather.base.core.experimental.ProgressBarHolder

interface LiveDataResourceReadyView : LifecycleOwner, ProgressBarHolder, DefaultErrorHandler