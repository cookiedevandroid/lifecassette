package com.life.lifecassette.core.platform

import com.life.lifecassette.domain.failures.ThrowableToFailureConverter
import core.platform.BaseCoreViewModel

abstract class BaseViewModel : BaseCoreViewModel() {

    lateinit var errorConverter: ThrowableToFailureConverter
}