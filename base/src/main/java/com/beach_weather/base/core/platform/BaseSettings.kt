package core.platform

import android.content.Context
import android.content.SharedPreferences

open class BaseSettings(context: Context, preferencesName: String) {
    private val preferences: SharedPreferences by lazy {
        context.getSharedPreferences(
            preferencesName,
            Context.MODE_PRIVATE
        );
    }

    fun prefString() = PrefStringDelegate(preferences)
    fun prefLong() = PrefLongDelegate(preferences)
    fun prefInt(defVal: Int = -1) = PrefIntDelegate(preferences, defVal)
    fun prefBoolean() = PrefBooleanDelegate(preferences)

    fun removeAll() {
        preferences.edit().clear().apply()
    }
}