package data.net.base

import data.net.NetErrorModel
import java.io.Serializable

open class BaseResponse<out T> : Serializable {
    val success: Boolean = false
    val data: T? = null
    val error: NetErrorModel? = null

    override fun toString(): String {
        return "BaseResponse(success=$success, data=$data, error=$error)"
    }
}

open class BaseResponseMock<out T> : Serializable {
    val success: Boolean = false
    val data: T? = null
    val error: String? = null

    override fun toString(): String {
        return "BaseResponse(success=$success, data=$data, error=$error)"
    }
}