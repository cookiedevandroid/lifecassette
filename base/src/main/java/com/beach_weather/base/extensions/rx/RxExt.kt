package extensions.rx

import android.os.Looper
import android.view.View
import com.beach_weather.base.R
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import io.reactivex.schedulers.Schedulers

infix operator fun CompositeDisposable.plus(disp: Disposable?) {
    disp?.let {
        this.add(disp)
    }
}

fun <T> Observable<T>.mainThread() = compose { observeOn(AndroidSchedulers.mainThread()) }
fun <T> Observable<T>.io() = compose { observeOn(Schedulers.io()) }
fun <T> Observable<T>.computation() = compose { observeOn(Schedulers.computation()) }

fun Completable.mainThread() = compose { observeOn(AndroidSchedulers.mainThread()) }
fun Completable.io() = compose { observeOn(Schedulers.io()) }
fun Completable.computation() = compose { observeOn(Schedulers.computation()) }

fun <T> Single<T>.mainThread() = compose { observeOn(AndroidSchedulers.mainThread()) }
fun <T> Single<T>.io() = compose { observeOn(Schedulers.io()) }
fun <T> Single<T>.subscribeOnio() = compose { subscribeOn(Schedulers.io()) }
fun <T> Single<T>.computation() = compose { observeOn(Schedulers.computation()) }

fun <T> Flowable<T>.mainThread() = compose { observeOn(AndroidSchedulers.mainThread()) }
fun <T> Flowable<T>.subscribeOnIo() = compose { subscribeOn(AndroidSchedulers.mainThread()) }
fun <T> Flowable<T>.io() = compose { observeOn(Schedulers.io()) }
fun <T> Flowable<T>.computation() = compose { observeOn(Schedulers.computation()) }

fun createRetryNetObs(view: View?) =
    RxModalScreen.snackBar(
        view,
        R.string.error_connection_fail,
        R.string.alert_action_retry
    )

fun createRetryNetSingle(view: View?) =
    RxModalScreen.snackBarForSingle(
        view,
        R.string.error_connection_fail,
        R.string.alert_action_retry
    )

fun initMainThreadScheduler() =
    RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }

fun checkMainThread(observer: Observer<*>): Boolean {
    if (Looper.myLooper() != Looper.getMainLooper()) {
        observer.onSubscribe(Disposables.empty())
        observer.onError(
            IllegalStateException(
                "Expected to be called on the main thread but was " + Thread.currentThread().name
            )
        )
        return false
    }
    return true
}