package com.life.lifecassette.utils.extentions

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.Service
import android.content.Context
import android.content.Context.ACTIVITY_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Point
import android.os.Build
import android.provider.Settings
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import com.life.lifecassette.BuildConfig

fun isLollipop() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
fun isMarshmallow() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
fun isOreo() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
fun isPie() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.P
fun isR() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.R

fun Context.getAppUsableScreenSize(): Point {
    val windowManager = this.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val display = windowManager.defaultDisplay
    val size = Point()
    display.getSize(size)
    return size
}

fun Context.isApplicationOnPause(): Boolean {
    val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    val tasks = activityManager.getRunningTasks(1)
    if (tasks.isNotEmpty()) {
        val topActivity = tasks[0].topActivity
        return topActivity?.packageName != packageName
    }
    return false
}

fun Context.shareText(text: String) {
    val sendIntent = Intent()
    sendIntent.action = Intent.ACTION_SEND
    sendIntent.putExtra(Intent.EXTRA_TEXT, text)
    sendIntent.type = "text/plain"
    this.startActivity(sendIntent)
}

fun Context.hideKeyboard() {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
}

@SuppressLint("HardwareIds")
fun Context.getSecureAndroidId() =
    Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)

fun Context.isServiceRunning(serviceClass: Class<out Service>) =
    (getSystemService(ACTIVITY_SERVICE) as ActivityManager)
        .getRunningServices(Int.MAX_VALUE)
        ?.map { it.service.className }
        ?.contains(serviceClass.name) ?: false

fun Context.hasTelephony() = this.packageManager.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)

fun isMarketReleaseBuild() = BuildConfig.FLAVOR == "market_" && !BuildConfig.DEBUG