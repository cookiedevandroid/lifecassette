package com.beach_weather.base.core.experimental

/**
 * Created by artCore on 3/18/19.
 */
interface ProgressBarHolder {
    fun showProgress(requestId: Int? = null)
    fun hideProgress(requestId: Int? = null)
}