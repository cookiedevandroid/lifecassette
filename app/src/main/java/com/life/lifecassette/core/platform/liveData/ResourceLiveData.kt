package com.beach.weather.core.platform.liveData

import androidx.lifecycle.LiveData
import core.exception.Failure

/**
 * Created by artCore on 30.10.19.
 */

/**
 * Extend LiveData to minimize effort when work with timeConsuming resource retrieving.
 * Also it is cool that client can't just cast this class to MutableLiveData and setValues different values.
 *
 * How it works.
 *
 * When started data loading call 'postLoading()' with optional data.
 * When data loaded call either postSuccess(data) or postError(failure, data)
 *
 * 'postFailure(f, d)' retrieves last value from current LiveData (if other data not set),
 * so you will always have your previously loaded data displayed.
 *
 * @see {@link LiveResource}
 * @see {@link https://github.com/android/architecture-components-samples/blob/master/GithubBrowserSample/app/src/main/java/com/android/example/github/vo/Resource.kt}
 *
 * @return {@link LiveData<LiveResource<T>}
 **/
class ResourceLiveData<T> : LiveData<LiveResource<T>>() {
    fun postSuccess(data: T?) {
        postValue(LiveResource.success(data))
    }

    @JvmOverloads
    fun postFailure(failure: Failure?, data: T? = null) {
        val finalData = data ?: value?.data
        postValue(
            LiveResource.error(failure, finalData)
        )
    }

    fun postLoading(data: T? = null) {
        val finalData = data ?: value?.data
        val result = LiveResource.loading(finalData)
        postValue(result)
    }

    fun reset() = postValue(null)

    fun isNull() = value?.data == null
}
