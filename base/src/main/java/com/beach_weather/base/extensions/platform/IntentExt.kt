package com.beach_weather.base.extensions.platform

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import extensions.platform.ISO_8601_FORMAT
import extensions.platform.dateFrom

fun Intent.addToCalendar(title: String?, date: String?): Intent = this.apply {
    action = Intent.ACTION_EDIT
    type = ACTION_TYPE
    putExtra(BEGIN_TIME_KEY, dateFrom(date ?: "", ISO_8601_FORMAT)?.time)
    putExtra(TITLE, title)
}

fun Intent.call(phone: String?): Intent = this.apply {
    action = Intent.ACTION_CALL
    data = Uri.parse("tel:$phone")
}

fun Intent?.openFacebook(pm: PackageManager, url: String): Intent? {
    return try {
        val applicationInfo = pm.getApplicationInfo(FACEBOOK_PACKAGE, 0)
        if (applicationInfo.enabled) {
            this.apply {
                this?.action = Intent.ACTION_VIEW
                this?.data = Uri.parse("$FACEBOOK_URL$url")
            }
        } else {
            null
        }
    } catch (e: Exception) {
        null
    }
}

fun Intent.openBrowser(url: String): Intent = this.apply {
    action = Intent.ACTION_VIEW
    data = Uri.parse(url)
}

fun Intent?.openTwitter(pm: PackageManager, url: String): Intent? {
    return try {
        val applicationInfo = pm.getApplicationInfo(TWITTER_PACKAGE, 0)
        if (applicationInfo.enabled) {
            this.apply {
                this?.action = Intent.ACTION_VIEW
                this?.data = Uri.parse(url)
            }
        } else {
            null
        }
    } catch (e: Exception) {
        null
    }
}

fun Intent.openMap(lat: Double, lon: Double, title: String?): Intent = this.apply {
    action = Intent.ACTION_VIEW
    data = Uri.parse("geo:$lat,$lon?q=$lat,$lon($title)")
    `package` = "com.google.android.apps.maps"
}

private const val ACTION_TYPE = "vnd.android.cursor.item/event"
private const val BEGIN_TIME_KEY = "beginTime"
private const val TITLE = "title"
private const val FACEBOOK_PACKAGE = "com.facebook.katana"
private const val TWITTER_PACKAGE = "com.twitter.android"
private const val FACEBOOK_URL = "fb://facewebmodal/f?href="
private const val TWITTER_URL = "fb://facewebmodal/f?href="
