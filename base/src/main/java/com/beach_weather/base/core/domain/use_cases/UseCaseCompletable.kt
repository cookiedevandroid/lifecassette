package com.beach_weather.base.core.domain.use_cases

import io.reactivex.Completable

abstract class UseCaseCompletable<in Params> : UseCase<Completable, Params>() {

    abstract override fun run(params: Params): Completable
}