package com.life.lifecassette

import android.app.Application
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes

class LifeCassetteApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initAnalytics()
    }

    private fun initAnalytics() {
        if (!BuildConfig.DEBUG) {
            AppCenter.start(this, BuildConfig.APP_CENTER_SECRET,
                Analytics::class.java, Crashes::class.java)
        }
    }
}