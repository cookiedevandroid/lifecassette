package com.beach.weather.utils

import extensions.rx.mainThread
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeUnit.SECONDS

class CountDownTime {
    private var timeUnit: TimeUnit = SECONDS
    private var disposable: Disposable? = null
    private var callBack: CallBack? = null
    var timeUntilFinished: Long? = null
        private set

    //Init block
    fun setTimeUnit(timeUnit: TimeUnit): CountDownTime {
        this.timeUnit = timeUnit
        return this
    }

    fun registerCallBack(callBack: CallBack): CountDownTime {
        this.callBack = callBack
        return this
    }
    //End init block

    fun start(startValue: Long) {
        Observable.zip(observableRange(startValue), observableInterval(), infoZipper(startValue))
            .subscribeOn(Schedulers.io())
            .mainThread()
            .subscribe(object : Observer<Long> {
                override fun onSubscribe(d: Disposable) {
                    disposable = d
                    callBack?.onTick(startValue)
                }

                override fun onNext(timeUntilFinished: Long) {
                    this@CountDownTime.timeUntilFinished = timeUntilFinished
                    callBack?.onTick(timeUntilFinished)
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                }

                override fun onComplete() {
                    callBack?.onFinish()
                }
            })
    }

    private fun observableRange(startValue: Long) = Observable.range(0, startValue.toInt())

    private fun observableInterval() = Observable.interval(1, timeUnit)

    private fun infoZipper(startValue: Long) = BiFunction<Int, Long, Long> { secondsPassed, _ ->
        val timeUntilFinished = startValue - secondsPassed
        timeUntilFinished
    }

    fun cancel() {
        if (disposable != null) {
            disposable!!.dispose()
            disposable = null
        }
    }

    companion object {
        const val TAG: String = "CountDownTime"

        fun instance(): CountDownTime = CountDownTime()
    }

    interface CallBack {
        fun onTick(tickValue: Long)
        fun onFinish()
    }
}
