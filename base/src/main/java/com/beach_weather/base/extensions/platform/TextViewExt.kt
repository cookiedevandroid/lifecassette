package com.beach_weather.base.extensions.platform

import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.beach_weather.base.R
import com.beach_weather.base.utils.TypefaceSpan

/**
 * Created by artCore on 4/16/19.
 */
inline fun TextView.doOnEditorActionPerformed(
    actionId: Int = EditorInfo.IME_ACTION_DONE,
    crossinline callback: () -> Unit
) {
    setOnEditorActionListener { _, _actionId, _ ->
        if (_actionId == actionId) {
            callback()
            true
        } else {
            false
        }
    }
}

fun TextView.makeLinks(vararg links: Pair<String, View.OnClickListener>) {
    val spannableString = SpannableString(this.text)
    for (link in links) {
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
                Selection.setSelection((view as TextView).text as Spannable, 0)
                view.invalidate()
                link.second.onClick(view)
            }
        }
        val startIndexOfLink = this.text.toString().indexOf(link.first)
        spannableString.setSpan(
            clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
    movementMethod = LinkMovementMethod.getInstance()
    setText(spannableString, TextView.BufferType.SPANNABLE)
}