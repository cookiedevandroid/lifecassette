package extensions

import android.graphics.Bitmap
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

fun Bitmap.writeToFile(file: File){
  try {
    FileOutputStream(file).use { out ->
      compress(Bitmap.CompressFormat.PNG, 100, out)
    }
  } catch (e: IOException) {
    e.printStackTrace()
  }
}