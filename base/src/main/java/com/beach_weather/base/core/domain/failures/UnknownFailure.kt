package com.beach_weather.base.core.domain.failures

import core.exception.Failure

class UnknownFailure(val message: String?) : Failure.FeatureFailure()