package com.beach_weather.base.extensions.platform

import android.view.MotionEvent
import android.view.View
import androidx.appcompat.widget.AppCompatEditText
import com.google.android.material.textfield.MaterialAutoCompleteTextView

fun AppCompatEditText.addRightDrawableListener(listener: () -> Unit) {
    setOnTouchListener(object : View.OnTouchListener {
        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            val right: Int = right
            val drawableSize: Int = compoundPaddingRight
            val x = event?.x?.toInt() ?: 0
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> if (x + 50 >= right - drawableSize && x <= right + 50) {

                    return true
                }
                MotionEvent.ACTION_UP -> {
                    if (x + 50 >= right - drawableSize && x <= right + 50) {
                        listener()
                        return true
                    }
                }
            }
            return false
        }
    })
}

fun MaterialAutoCompleteTextView.addRightDrawableListener(listener: () -> Unit) {
    setOnTouchListener(object : View.OnTouchListener {
        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            val right: Int = right
            val drawableSize: Int = compoundPaddingRight
            val x = event?.x?.toInt() ?: 0
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> if (x + 50 >= right - drawableSize && x <= right + 50) {

                    return true
                }
                MotionEvent.ACTION_UP -> {
                    if (x + 50 >= right - drawableSize && x <= right + 50) {
                        listener()
                        return true
                    }
                }
            }
            return false
        }
    })
}

fun AppCompatEditText.addActionListener(action: Int, listener: () -> Unit) {
    setOnEditorActionListener { _, actionId, _ ->
        if (actionId == action) {
            listener()
            return@setOnEditorActionListener true
        }
        return@setOnEditorActionListener false
    }
}