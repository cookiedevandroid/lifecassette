import android.os.Parcel
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import network.features.user.CurrentUserResponse
import org.hamcrest.core.Is.`is`
import org.hamcrest.core.IsEqual
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by artCore on 08.08.19.
 */
@RunWith(AndroidJUnit4::class)
class CurrentUserResponseParcelTest  {

    @Test
    @SmallTest
    fun parcelTest() {
        val user = CurrentUserResponse(
            id = 12,
            permissions = listOf("Read", "Write"),
            roles = listOf("Pal", "Tech")
        )
        val parcel = Parcel.obtain()
        user.writeToParcel(parcel, user.describeContents())
        parcel.setDataPosition(0)
        val createdFromParcel = CurrentUserResponse.CREATOR.createFromParcel(parcel)
//
        assertThat(createdFromParcel.id, `is`(12))
        assertThat(createdFromParcel.permissions, IsEqual(listOf("Read", "Write")))
        assertThat(createdFromParcel.roles, IsEqual(listOf("Pal", "Tech")))
    }
}