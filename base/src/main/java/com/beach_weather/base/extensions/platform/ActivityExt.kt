@file:JvmName("ActivityExt")

package com.beach_weather.base.extensions.platform

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.net.Uri
import android.provider.Settings
import android.util.TypedValue
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import kotlin.math.roundToInt

/**
 * Created by artCore on 4/15/19.
 */

fun AppCompatActivity.requestPermissions(permissions: Array<String>, requestCode: Int) {
    ActivityCompat.requestPermissions(this, permissions, requestCode)
}

fun FragmentActivity.mainNavController(fragmentId: Int): NavController =
    (supportFragmentManager.findFragmentById(fragmentId) as NavHostFragment).navController

fun Activity.openAppSettings() {
    val intent = Intent()
    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
    val uri = Uri.fromParts("package", packageName, null)
    intent.data = uri
    startActivity(intent)
}

fun Activity.statusBarHeight(): Int {
    val rectangle = Rect()
    val window = window
    window.decorView.getWindowVisibleDisplayFrame(rectangle)
    val statusBarHeight = rectangle.top
//    val contentViewTop = window.findViewById<View>(Window.ID_ANDROID_CONTENT).top
//    val titleBarHeight = contentViewTop -statusBarHeight

    return statusBarHeight
}

fun Activity.getRootView(): View {
    return findViewById<View>(android.R.id.content)
}

fun Context.convertDpToPx(dp: Float): Float {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dp,
        this.resources.displayMetrics
    )
}

fun Activity.isKeyboardOpen(): Boolean {
    val visibleBounds = Rect()
    this.getRootView().getWindowVisibleDisplayFrame(visibleBounds)
    val heightDiff = getRootView().height - visibleBounds.height()
    val marginOfError = this.convertDpToPx(50F).roundToInt()
    return heightDiff > marginOfError
}

fun Activity.isKeyboardClosed(): Boolean {
    return !this.isKeyboardOpen()
}

fun Activity.applyTheme(theme: Int) {
    this.applicationContext.setTheme(theme)
    this.setTheme(theme)
}

fun Activity.isKeyboardVisible(): Boolean {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    return imm.isActive
}

fun Activity.hideSoftKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun AppCompatActivity.showOrReplaceLast(tag: String, replaceFunc: FragmentTransaction.() -> Unit) {
    val manager = supportFragmentManager
    val fragment = manager.findFragmentByTag(tag)
    if (fragment?.isHidden == true) {
        manager.beginTransaction().show(fragment).commit()
    }

    if (fragment == null || !fragment.isVisible) {
        if (!isFinishing) {
            val transaction = manager.beginTransaction()
            transaction.replaceFunc()
            transaction.commit()
        }
    }
}

fun AppCompatActivity.executeIfNotVisible(
    tag: String,
    init: FragmentTransaction.(tag: String) -> Unit
) {
    val fragment = supportFragmentManager.findFragmentByTag(tag)
    if (fragment == null || !fragment.isVisible) {
        if (!isFinishing) {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.init(tag)
            transaction.commit()
        }
    }
}

fun AppCompatActivity.fragmentTransaction(transactioFun: FragmentTransaction.() -> Unit) {
    val transaction = supportFragmentManager.beginTransaction()
    transaction.transactioFun()
    transaction.commit()
}

inline fun <reified T : Fragment> AppCompatActivity.fragmentById(id: Int): T? =
    supportFragmentManager.findFragmentById(id) as? T

inline fun <reified T : Fragment> AppCompatActivity.fragmentByTag(tag: String): T? =
    supportFragmentManager.findFragmentByTag(tag) as? T