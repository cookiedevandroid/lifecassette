package com.life.lifecassette.utils.extentions

import java.util.*

fun Double.format(digits: Int) = "%.${digits}f".format(Locale.US, this)

fun Int?.toPrice() : Double = ((this?.toDouble() ?: 0.0) / 100)