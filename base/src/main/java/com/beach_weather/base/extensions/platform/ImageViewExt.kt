package com.beach_weather.base.extensions.platform

import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

fun ImageView.load(@DrawableRes resource: Int) {
    Glide.with(this)
        .load(resource)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(this)
}

fun ImageView.load(path: String, @DrawableRes placeHolder: Int, cornerRadius: Int) {
    Glide.with(this)
        .load(path)
        .placeholder(placeHolder)
        .transform(CenterCrop(), RoundedCorners(cornerRadius))
        .into(this)
}

fun ImageView.loadCrop(path: String, @DrawableRes placeHolder: Int) {
    Glide.with(this)
        .load(path)
        .placeholder(placeHolder)
        .transform(CenterCrop())
        .into(this)
}

fun ImageView.load(path: String) {
    Glide.with(this)
        .load(path)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(this)
}

fun ImageView.load(path: String, @DrawableRes placeHolder: Int) {
    Glide.with(this)
        .load(path)
        .placeholder(placeHolder)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(this)
}