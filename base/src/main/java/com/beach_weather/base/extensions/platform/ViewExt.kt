@file:JvmName("ViewExt")
@file:Suppress("unused")

package com.beach_weather.base.extensions.platform

import android.animation.*
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.UnderlineSpan
import android.view.*
import android.view.animation.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.RatingBar
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.ListPopupWindow
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.view.isVisible
import androidx.transition.AutoTransition
import androidx.transition.Transition
import androidx.transition.TransitionManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import extensions.platform.*
import kotlin.math.hypot

/**
 * Created by artCore on 18.11.15.
 */

fun TextView.isEmpty(): Boolean = TextUtils.isEmpty(this.text.toString())

fun TextView.isNotEmpty(): Boolean = TextUtils.isEmpty(this.text.toString()).not()
fun View.show() {
    this.visibility = (View.VISIBLE)
}

fun View.showIf(condition: Boolean) {
    if (condition) this.visibility = (View.VISIBLE)
}

fun View.manageVisibility(condition: Boolean) {
    if (condition) this.visibility = (View.VISIBLE)
    else visibility = View.GONE
}

fun View.manageVisibilityInvisible(condition: Boolean) {
    if (condition) this.visibility = (View.VISIBLE)
    else visibility = View.INVISIBLE
}

fun View.slideDown() {
    val animate = TranslateAnimation(0F, 0F, 0F, (-height).toFloat())
    animate.duration = 500
    animate.fillAfter = true
    startAnimation(animate)
    visibility = View.GONE
}

fun View.showPopup(
    values: Array<String>,
    adapter: ArrayAdapter<String>,
    dismissLast: Boolean = true,
    itemSelected: (String) -> Unit,
    style: Int,
    dismiss: () -> Unit,
    manageAddress: () -> Unit?
) {
    val listPopupWindow = ListPopupWindow(context, null, style)

    listPopupWindow.setAdapter(adapter)
    listPopupWindow.anchorView = this
    listPopupWindow.isModal = true
    listPopupWindow.setOnItemClickListener { _, _, position, _ ->
        if (dismissLast) {
            if (position != values.size - 1) itemSelected(values[position]) else manageAddress()
        } else {
            itemSelected(values[position])
        }
        listPopupWindow.dismiss()
    }
    listPopupWindow.setOnDismissListener {
        dismiss.invoke()
    }

    listPopupWindow.show()
}

fun View.makeInvisibleIfNot(condition: Boolean) {
    if (condition) this.visibility = (View.VISIBLE)
    else visibility = View.INVISIBLE
}

fun View.hide() {
    this.visibility = (View.GONE)
}

fun View.invisible() {
    this.visibility = (View.INVISIBLE)
}

fun View.isVisible(): Boolean = this.isVisible

fun View.isNotVisible(): Boolean = this.visibility != (View.VISIBLE)

val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()
val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

fun View.toggleVisibility() = if (isVisible()) hide() else show()

fun View.toggleVisibilityAnim(duration: Long = 200) {
    if (isVisible()) hideAnim(duration) else showAnim(duration)
}

@JvmOverloads
fun View.showAnim(duration: Long = 200) {
    this.alpha = 0f
    this.isVisible = true
    this.animate()
        .setDuration(duration)
        .alpha(1f)
        .setListener(null)
}

@JvmOverloads
fun View.hideAnim(duration: Long = 200, endListener: (() -> Unit)? = null) {
    this.animate()
        .setDuration(duration)
        .alpha(0f)
        .setListener(animOnFinish {
            this.isVisible = false
            endListener?.invoke()
        })
}

fun View.invisibleAnim(duration: Long = 200, endListener: (() -> Unit)? = null) {
    this.animate()
        .setDuration(duration)
        .alpha(0f)
        .setListener(animOnFinish {
            endListener?.invoke()
            this.visibility = View.INVISIBLE
        })
}

fun View.getDrawerBitmap(): Bitmap {
    val b = Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.RGB_565)
    val c = Canvas(b)
    layout(left, top, right, bottom)
    draw(c)
    return b
}

fun View.onClick(listener: View.OnClickListener) {
    this.setOnClickListener { listener.onClick(this) }
}

fun View.onClick(listener: ((View) -> Unit)?) {
    this.setOnClickListener { listener?.invoke(it) }
}

fun View.lock() {
    isEnabled = false
    isClickable = false
}

fun View.unlock() {
    isEnabled = true
    isClickable = true
}

fun ViewGroup.lockAllChildren() {
    views().forEach { it.lock() }
}

fun ViewGroup.unlockAllChildren() {
    views().forEach { it.unlock() }
}

fun View.doOnPreDraw(callback: () -> Unit) {
    viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
        override fun onPreDraw(): Boolean {
            viewTreeObserver.removeOnPreDrawListener(this)
            callback.invoke()
            return true
        }
    })
}

fun View.adjustBottomPositionDueToNavBar() {
    val layoutParams = layoutParams as ViewGroup.MarginLayoutParams
    val marginStart = layoutParams.marginStart
    val marginEnd = layoutParams.marginEnd
    val topMargin = layoutParams.topMargin
    val bottom = layoutParams.bottomMargin + context.getNavigationBarHeight()

    layoutParams.setMargins(marginStart, topMargin, marginEnd, bottom)
}

operator fun ViewGroup.get(pos: Int): View = getChildAt(pos)

fun ViewGroup.views(): List<View> = (0 until childCount).map { getChildAt(it) }

inline fun View.snack(message: String, f: Snackbar.() -> Unit, length: Int = Snackbar.LENGTH_LONG) {
    val snack = Snackbar.make(this, message, length)
    snack.f()
    snack.show()
}

inline fun View.snack(
    resId: Int,
    f: Snackbar.() -> Unit,
    @IdRes length: Int = Snackbar.LENGTH_LONG
) {
    val snack = Snackbar.make(this, resId, length)
    snack.f()
    snack.show()
}

fun TextView.stringText() = this.text.toString()
fun TextView.clearText() {
    this.text = ""
}

fun EditText.clearText() {
    text.clear()
}

fun EditText.moveCursorToEnd() {
    if (text.isNotEmpty()) setSelection(text.length)
}

fun View.showKeyBord() {
    this.requestFocus()
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}

fun View.showKeyBoardImplicit() {
    this.requestFocus()
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
}

fun EditText.getString() = this.text.toString()

inline fun View.waitForLayout(crossinline f: () -> Unit) = with(viewTreeObserver) {
    addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            removeOnGlobalLayoutListener(this)
            f()
        }
    })
}

inline fun View.onGlobalLayout(crossinline action: () -> Unit) {
    viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            viewTreeObserver.removeOnGlobalLayoutListener(this)
            action()
        }
    })
}

@JvmOverloads
fun View.changeVisibilityStateRespect(
    shouldShow: Boolean,
    animate: Boolean = false,
    boundAction: (() -> Unit)? = null
) {
    if (shouldShow) {
        if (isNotVisible()) {
            boundAction?.invoke()
            if (animate) showAnim()
            else show()
        }
    } else {
        if (isVisible()) {
            boundAction?.invoke()
            if (animate) hideAnim()
            else hide()
        }
    }
}

fun TextView.showOnlyIfNotEmpty() {
    manageVisibility(this.text.isNotBlank())
}

fun ViewGroup.transition(duration: Long = 200, transition: Transition = AutoTransition()) {
    transition.duration = duration
    TransitionManager.beginDelayedTransition(this, transition)
}

fun View.animateBackgroundColor(
    @ColorRes colorFromRes: Int,
    @ColorRes colorToResId: Int,
    duration: Long = 260,
    endListener: (() -> Unit)? = null
) {
    val colorToRes = this.context.getColorCompat(colorToResId)
    val colorFrom = this.context.getColorCompat(colorFromRes)

    animateBackgroundColor(duration, from = colorFrom, to = colorToRes, endListener = endListener)
}

fun View.animateBackgroundColor(
    duration: Long = 500L,
    from: Int,
    to: Int,
    endListener: (() -> Unit)? = null
) {
    val anim = ValueAnimator()
    anim.setIntValues(from, to)
    anim.setEvaluator(ArgbEvaluator())
    anim.addUpdateListener { valueAnimator -> this.setBackgroundColor(valueAnimator.animatedValue as Int) }

    anim.duration = duration
    endListener?.let {
        anim.addListener(animOnFinish(endListener))
    }
    anim.start()
}

fun View.getCurrentBackgroundColor(): Int? = (this.background as? ColorDrawable)?.color

fun View.animateColorWhatever(
    duration: Long = 500L,
    colorFrom: Int,
    colorTo: Int,
    animation: (ValueAnimator) -> Unit
) {
    val anim = ValueAnimator()
    anim.setIntValues(colorFrom, colorTo)
    anim.setEvaluator(ArgbEvaluator())
    anim.addUpdateListener { valueAnimator -> animation(valueAnimator) }

    anim.duration = duration
    anim.start()
}

fun View.changeColor(to: Int) {
    if (this.getCurrentBackgroundColor() != to) {
        ValueAnimator().apply {
            setIntValues(this@changeColor.getCurrentBackgroundColor() ?: 0, to)
            setEvaluator(ArgbEvaluator())
            addUpdateListener { valueAnimator -> this@changeColor.setBackgroundColor(valueAnimator.animatedValue as Int) }
            duration = 50L
            start()
        }
    }
}

fun TextInputLayout.setErrorMess(
    isValid: Boolean,
    errorStringId: Int,
    manageErrEnableChanges: Boolean = true
) {
    if (isValid) {
        error = null
        if (manageErrEnableChanges) isErrorEnabled = false
    } else {
        if (manageErrEnableChanges) isErrorEnabled = true
        error = this.context.getString(errorStringId)
    }
}

fun View.animateHSVBackground(
    colorFrom: Int,
    colorTo: Int,
    duration: Long = 500L,
    endListener: (() -> Unit)? = null
) {
    val from = FloatArray(3)
    val to = FloatArray(3)

    Color.colorToHSV(colorFrom, from)   // from white
    Color.colorToHSV(colorTo, to)     // to red

    val anim = ValueAnimator.ofFloat(0F, 1F)   // animate from 0 to 1
    anim.duration = duration                             // for 300 ms

    val hsv = FloatArray(3)                  // transition color
    anim.addUpdateListener { animation ->
        // Transition along each axis of HSV (hue, saturation, value)
        hsv[0] = from[0] + (to[0] - from[0]) * animation.animatedFraction
        hsv[1] = from[1] + (to[1] - from[1]) * animation.animatedFraction
        hsv[2] = from[2] + (to[2] - from[2]) * animation.animatedFraction

        this.setBackgroundColor(Color.HSVToColor(hsv))
    }
    endListener?.let {
        anim.addListener(animOnFinish(endListener))
    }
    anim.start()
}

@SuppressLint("ObjectAnimatorBinding")
fun FloatingActionButton.animateBackgroundColorFab(
    @ColorRes startColor: Int, @ColorRes endColor: Int,
    duration: Long = 800
) {

    val animator = ObjectAnimator.ofInt(
        this, "backgroundTint",
        context.getColorCompat(startColor), context.getColorCompat(endColor)
    )
    animator.duration = duration
    animator.setEvaluator(ArgbEvaluator())
    animator.interpolator = DecelerateInterpolator()
    animator.addUpdateListener { animation ->
        val animatedValue = animation.animatedValue as Int
        this.backgroundTintList = ColorStateList.valueOf(animatedValue)
    }
    animator.start()
}

fun EditText.onDoneTap(callback: () -> Unit) = onKeyTap(EditorInfo.IME_ACTION_DONE, callback)
fun EditText.onNextTap(callback: () -> Unit) = onKeyTap(EditorInfo.IME_ACTION_NEXT, callback)
fun EditText.onSearchTap(callback: () -> Unit) = onKeyTap(EditorInfo.IME_ACTION_SEARCH, callback)

private fun EditText.onKeyTap(imeKeyCode: Int, callback: () -> Unit) {
    imeOptions = imeKeyCode
    setOnEditorActionListener { _, actionId, _ ->
        return@setOnEditorActionListener if (actionId == imeKeyCode) {
            callback(); true
        } else false
    }
}

fun TextView.setDrawableStart(@DrawableRes icon: Int) {
    val drawable = AppCompatResources.getDrawable(this.context, icon)
    setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)
}

fun TextView.removeDrawables() {
    setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
}

fun TextView.setDrawableTop(@DrawableRes icon: Int) {
    val drawable = AppCompatResources.getDrawable(this.context, icon)
    setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null)
}

fun TextView.setDrawableEnd(@DrawableRes icon: Int? = null) {
    val drawable = icon?.let { AppCompatResources.getDrawable(this.context, icon) }
    setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)
}

fun <T : Drawable> TextView.setDrawableEnd(drawable: T) {
    setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)
}

fun TextView.setDrawableBottom(@DrawableRes icon: Int) {
    val drawable = AppCompatResources.getDrawable(this.context, icon)
    setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable)
}

fun RatingBar.setStarsColorRes(@ColorRes colorRes: Int) {
    val stars = progressDrawable
    val color = context.getColorCompat(colorRes)
    hasLollipop { DrawableCompat.setTint(stars, color) }
    doForPreLollipop { stars.setColorFilter(color, PorterDuff.Mode.SRC_ATOP) }
}

fun ViewGroup.inflate(layoutRes: Int): View =
    LayoutInflater.from(context).inflate(layoutRes, this, false)

fun EditText.hideKeyBord() {
    val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputManager.hideSoftInputFromWindow(this.windowToken, 0)
}

fun View.hideKeyBord() {
    val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputManager.hideSoftInputFromWindow(
        this.windowToken, 0
    )
    clearFocus()
}

fun View.hideAnimWithScale(
    duration: Long = 200,
    interpolator: Interpolator = AccelerateDecelerateInterpolator()
) {
    if (this.isVisible()) {
        this.animate()
            .alpha(0f)
            .scaleX(0f)
            .scaleY(0f)
            .setDuration(duration)
            .setInterpolator(interpolator)
            .withEndAction { this.hide() }
            .start()
    }
}

fun View.showAnimWithScale(
    duration: Long = 200,
    interpolator: Interpolator = AccelerateDecelerateInterpolator()
) {
    if (this.isNotVisible()) {
        doOnPreDraw {
            alpha = 0f
            scaleY = 0f
            scaleX = 0f
            this.show()
            this.animate()
                .alpha(1f)
                .scaleY(1f)
                .scaleX(1f)
                .setInterpolator(interpolator)
                .setDuration(duration)
                .start()
        }
    }
}

fun View.showAnimWithScaleIf(show: Boolean, duration: Long = 200, interpolator: Boolean = false) {
    if (show) showAnimWithScale(
        duration,
        if (interpolator) OvershootInterpolator() else AccelerateDecelerateInterpolator()
    )
    else hideAnimWithScale(
        duration,
        if (interpolator) AnticipateInterpolator() else AccelerateDecelerateInterpolator()
    )
}

@Suppress("unused", "UNUSED_PARAMETER")
@TargetApi(21)
fun View.showAnimWithReveal(
    duration: Long = 200,
    endAction: () -> Unit = {},
    x: Int = -1,
    y: Int = -1
) {
    if (isNotVisible()) {
        this.doOnPreDraw {
            alpha = 1f
            translationY = 0f
            translationX = 0f
            val centerX = if (x >= 0) x else width / 2
            val centerY = if (y >= 0) y else height / 2
            val finalRadius = hypot(centerX.toDouble(), centerY.toDouble())

            val revealAnim = ViewAnimationUtils.createCircularReveal(
                this,
                centerX,
                centerY,
                0f,
                finalRadius.toFloat()
            )
            revealAnim.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(p0: Animator?) {
                    endAction()
                }
            })

            show()
            revealAnim.start()
        }
    } else {
        endAction()
    }
}

@TargetApi(21)
fun View.hideAnimWithReveal(
    @Suppress("UNUSED_PARAMETER") duration: Long = 200,
    endAction: () -> Unit = {},
    x: Int = -1,
    y: Int = -1
) {
    if (isVisible()) {
        val cx = if (x >= 0) x else width / 2
        val cy = if (y >= 0) y else height / 2
        val initialRadius = hypot(cx.toDouble(), cy.toDouble())

        val revealAnim =
            ViewAnimationUtils.createCircularReveal(this, cx, cy, initialRadius.toFloat(), 0f)
        revealAnim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                hide()
                endAction()
            }
        })

        revealAnim.start()
    } else {
        endAction()
    }
}

@SuppressLint("RestrictedApi")
fun View.animateIn(duration: Long = 200) {
    if (this.isNotVisible()) {
        doOnPreDraw {
            visibility = View.VISIBLE
            alpha = 0.5f
            scaleX = 0f
            scaleY = 0f
            animate()
                .alpha(1f)
                .scaleX(1f)
                .scaleY(1f)
                .setInterpolator(OvershootInterpolator())
                .setDuration(duration)
                .start()
        }
    }
}

fun View.animateOut(duration: Long = 200) {
    if (this.isVisible()) {
        animate()
            .alpha(0.5f)
            .scaleX(0f)
            .scaleY(0f)
            .withEndAction { invisible() }
            .setInterpolator(AnticipateInterpolator())
            .setDuration(duration)
            .start()
    }
}

fun View.blink(action: (() -> Unit)? = null) {

    this.alpha = 1f
    this.animate()
        .alpha(0f)
        .setDuration(200)
        .withEndAction {
            action?.invoke()
            this.animate()
                .alpha(1f)
                .setDuration(200)
                .start()
        }
        .start()
}

fun View.showAnimWithSlideUp(
    duration: Long = 300,
    withAlpha: Float = 0.5F,
    endAction: () -> Unit = {},
    interpolator: Interpolator = DecelerateInterpolator()
) {
    if (isNotVisible()) {
        show()
        this.doOnPreDraw {
            translationY = height.toFloat()
            alpha = withAlpha
            animate()
                .alpha(1f)
                .translationY(0f)
                .withEndAction(endAction)
                .setInterpolator(interpolator)
                .setDuration(duration)
                .start()
        }
    } else {
        endAction()
    }
}

fun View.showAnimWithSlideDown(
    duration: Long = 600,
    withAlpha: Float = 0.5F,
    endAction: () -> Unit = {},
    interpolator: Interpolator = DecelerateInterpolator()
) {
    if (isNotVisible()) {
        show()
        this.doOnPreDraw {
            translationY = -height.toFloat()
            alpha = withAlpha
            animate()
                .alpha(1f)
                .translationY(0f)
                .withEndAction(endAction)
                .setInterpolator(interpolator)
                .setDuration(duration)
                .start()
        }
    } else {
        endAction()
    }
}

fun View.hideAnimWithSlideDown(
    duration: Long = 300,
    withAlpha: Float = 0.5F,
    endAction: () -> Unit = {}
) {
    if (isVisible()) {
        animate()
            .alpha(withAlpha)
            .translationY(height.toFloat())
            .withEndAction {
                hide()
                endAction()
            }
            .setInterpolator(AccelerateInterpolator())
            .setDuration(duration)
            .start()
    } else {
        endAction()
    }
}

fun View.hideAnimWithSlideUp(
    duration: Long = 600,
    withAlpha: Float = 0.5F,
    endAction: () -> Unit = {}
) {
    if (isVisible()) {
        animate()
            .alpha(withAlpha)
            .translationY(-height.toFloat())
            .withEndAction {
                hide()
                endAction()
            }
            .setInterpolator(AccelerateDecelerateInterpolator())
            .setDuration(duration)
            .start()
    } else {
        endAction()
    }
}

fun View.invisibleAnimWithSlideDown(duration: Long = 300, endAction: () -> Unit = {}) {
    if (isVisible()) {
        animate()
            .alpha(0.5f)
            .translationY(height.toFloat())
            .withEndAction {
                invisible()
                endAction()
            }
            .setInterpolator(AccelerateInterpolator())
            .setDuration(duration)
            .start()
    } else {
        endAction()
    }
}

fun TextView.underlineText(string: String = this.text.toString()) {
    val content = SpannableString(string)
    content.setSpan(UnderlineSpan(), 0, content.length, 0)
    text = content
}

fun TextView.underlineText(@StringRes resId: Int) {
    underlineText(resources.getString(resId))
}

@TargetApi(Build.VERSION_CODES.M)
fun View.clickEffectCompat(@DrawableRes resource: Int) {
    if (hasMarshMellow()) {
        foreground = context.getDrawable(resource)
    } else {
        background = context.getDrawable(resource)
    }
}