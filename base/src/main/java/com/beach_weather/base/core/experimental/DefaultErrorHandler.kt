package core.experimental

import core.exception.Failure

interface DefaultErrorHandler {
    fun handleFailure(failure: Failure)
}