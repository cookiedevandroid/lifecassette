package com.life.lifecassette.core.platform

import android.os.Bundle
import com.life.lifecassette.utils.extentions.screenslog
import core.platform.BaseCoreFragmentDialog

abstract class BaseFragmentDialog: BaseCoreFragmentDialog() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        screenslog(this.javaClass.canonicalName ?: "null")
    }

    override fun onResume() {
        super.onResume()
        adjustDialogSize()
    }
}
