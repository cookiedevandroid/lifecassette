package extensions.platform

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.content.Context
import android.transition.Transition
import android.util.Log
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat

fun animateColor(
    context: Context, target: Any?, propertyName: String,
    @ColorRes colorFrom: Int, @ColorRes colorTo: Int, duration: Int = 400, callback: () -> Unit
) {
    val anim = ObjectAnimator.ofObject(
        target, propertyName,
        ArgbEvaluator(), ContextCompat.getColor(context, colorFrom),
        ContextCompat.getColor(context, colorTo)
    )
    anim.duration = duration.toLong()
    anim.addListener(animOnFinish { callback.invoke() })
    anim.start()
}

fun animOnFinish(callback: () -> Unit): AnimatorListenerAdapter {
    return object : AnimatorListenerAdapter() {
        override fun onAnimationEnd(animation: Animator?) {
            super.onAnimationEnd(animation)
            callback.invoke()
        }
    }
}

@Suppress("unused")
fun animOnStart(callback: () -> Unit): AnimatorListenerAdapter {
    return object : AnimatorListenerAdapter() {
        override fun onAnimationStart(animation: Animator?) {
            super.onAnimationStart(animation)
            callback.invoke()
        }
    }
}

fun simpleTransitionListener(
    transitionStart: ((Transition?) -> Unit)? = null,
    transitionEnd: ((Transition?) -> Unit)? = null
): Transition.TransitionListener {
    return object : Transition.TransitionListener {
        override fun onTransitionResume(transition: Transition?) {}
        override fun onTransitionPause(transition: Transition?) {}
        override fun onTransitionCancel(transition: Transition?) {}
        override fun onTransitionStart(transition: Transition?) {
            transitionStart?.invoke(transition)
        }

        override fun onTransitionEnd(transition: Transition?) {
            transitionEnd?.invoke(transition)
        }
    }
}

class AnimFinishListener private constructor(
    private val startCallback: ((Animator) -> Unit)?,
    private val endCallback: ((Animator) -> Unit)?,
    private val resumeCallback: ((Animator) -> Unit)?
) : AnimatorListenerAdapter() {

    companion object {
        fun builder() = Builder()

//      fun builder1() : Builder.() -> Unit {
//
//      }
    }

    class Builder {
        private var startCallback: (Animator) -> Unit = {}
        private var endCallback: (Animator) -> Unit = {}
        private var resumeCallback: (Animator) -> Unit = {}

        fun onStart(onStart: (Animator) -> Unit): Builder {
            startCallback = onStart
            return this
        }

        fun onResume(onResume: (Animator) -> Unit): Builder {
            resumeCallback = onResume
            return this
        }

        fun onEnd(onEnd: (Animator) -> Unit): Builder {
            endCallback = onEnd
            return this
        }

        fun build() = AnimFinishListener(startCallback, endCallback, resumeCallback)
    }

    @Suppress("MemberVisibilityCanBePrivate")
    var isFinished: Boolean = false
        private set

    override fun onAnimationCancel(animation: Animator) {
        super.onAnimationCancel(animation)
        isFinished = false
    }

    override fun onAnimationResume(animation: Animator) {
        super.onAnimationResume(animation)
        resumeCallback?.invoke(animation)
        Log.d("myLogs", "anim on resume")

    }

    override fun onAnimationPause(animation: Animator?) {
        super.onAnimationPause(animation)
        Log.d("myLogs", "anim on pause")
    }

    override fun onAnimationStart(animation: Animator) {
        super.onAnimationStart(animation)
        isFinished = false
        startCallback?.invoke(animation)
        Log.d("myLogs", "anim on start")

    }

    override fun onAnimationEnd(animation: Animator) {
        super.onAnimationEnd(animation)
        isFinished = true
        endCallback?.invoke(animation)
        Log.d("myLogs", "anim on end")
    }
}