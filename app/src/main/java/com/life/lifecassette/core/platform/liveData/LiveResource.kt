package com.beach.weather.core.platform.liveData

import com.beach.weather.core.platform.liveData.Status.ERROR
import com.beach.weather.core.platform.liveData.Status.LOADING
import com.beach.weather.core.platform.liveData.Status.SUCCESS
import core.exception.Failure

/**
 * Created by artCore on 30.10.19.
 */
data class LiveResource<out T>(val status: Status, val data: T?, val failure: Failure?) {
    fun onData(func: (T?) -> Unit){
        if (status == SUCCESS) { func(data) }
    }

    fun onDataNotNull(func: (T) -> Unit) {
        if (status == SUCCESS && data != null) { func(data) }
    }

    fun onFailure(call: (Failure, T?) -> Unit) {
        val f = failure
        if (f != null) {
            call(f, data)
        }
    }

    fun onLoadStarted(call: (T?) -> Unit) {
        if (status == LOADING) { call(data) }
    }

    fun onLoadFinished (call: (T?) -> Unit) {
        if (status != LOADING) { call(data) }
    }

    companion object {
        fun <T> success(data: T?): LiveResource<T> {
            return LiveResource(
                SUCCESS,
                data,
                null
            )
        }

        fun <T> error(failure: Failure?, data: T?): LiveResource<T> {
            return LiveResource(
                ERROR,
                data,
                failure
            )
        }

        fun <T> loading(data: T?): LiveResource<T> {
            return LiveResource(
                LOADING,
                data,
                null
            )
        }
    }
}

enum class Status {
    SUCCESS, LOADING, ERROR
}