package extensions.platform

import android.app.Activity
import android.os.Build
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.*

/**
 * Created by artCore on 5/17/18.
 */

internal const val ZERO = 0
internal const val SCROLL_PIXELS_IMPER = 80
internal const val SCROLL_PIXELS_MAX = 1000

fun RecyclerView.hideKeyboardOnScroll(scrollToBottomAmount: Int = SCROLL_PIXELS_IMPER) {
    addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if ((-dy) >= scrollToBottomAmount) (context as? Activity)?.hideKeyBoard()
        }
    })
}

fun RecyclerView.changeToolbarColor(
    scrollToBottomMin: Int = 20,
    scrollToBottomMax: Int = SCROLL_PIXELS_MAX,
    changeColor: () -> Unit,
    makeTransparent: () -> Unit
) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if ((computeVerticalScrollOffset()) == ZERO) {
                makeTransparent()
            }
            if (oldScrollY >= -40) {
                changeColor()
            }
        }
    } else {
        addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if ((recyclerView.computeVerticalScrollOffset()) == ZERO && dy != ZERO) makeTransparent()
                if ((computeVerticalScrollOffset() in scrollToBottomMin..scrollToBottomMax)) changeColor()
                super.onScrolled(recyclerView, dx, dy)
            }
        })
    }
}
