package com.beach_weather.base.core.domain.failures.base

import com.beach_weather.base.core.data.mappers.Mapper
import com.beach_weather.base.core.domain.failures.UnknownFailure
import com.beach_weather.base.core.domain.failures.*
import core.exception.Failure
import network.core.experimental.RetrofitException

abstract class BaseThrowableToFailureConverter  : Mapper<Throwable?, Failure> {

    override fun map(input: Throwable?): Failure {
        return when (input) {
            is RetrofitException -> mapRetrofitException(input)
            else -> UnknownFailure(input?.message)
        }
    }

    protected fun mapRetrofitException(input: RetrofitException) = when (input.kind) {
        RetrofitException.Kind.NETWORK -> NetworkConnectionFailure()
        RetrofitException.Kind.HTTP -> HttpFailure(input.netError?.msg)
        RetrofitException.Kind.CONVERSION -> ParseFailure()
        RetrofitException.Kind.UNEXPECTED -> UnknownFailure(null)
    }
}