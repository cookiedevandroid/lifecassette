package com.life.lifecassette.utils.extentions

import android.util.Log
import com.orhanobut.logger.Logger

const val MY_LOGS = "myLogs"
const val SCREENS = "screens"

fun isLogsEnabled() = !isMarketReleaseBuild()

fun prettyLog(msg: Any) {
    if (isLogsEnabled()) Logger.d(msg)
}

fun screenslog(mess: String, tag: String = SCREENS) {
    if (isLogsEnabled()) {
        Log.d(tag, mess)
    }
}

@JvmOverloads fun logd(mess: String, tag: String = MY_LOGS) {
    if (isLogsEnabled()) {
        Log.d(tag, mess)
    }
}

@JvmOverloads fun loge(mess: String, tag: String = MY_LOGS) {
    if (isLogsEnabled()) {
        Log.e(tag, mess)
    }
}

fun prettyLog(msg: String): Unit = Logger.d(msg)