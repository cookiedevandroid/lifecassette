package com.beach_weather.base.core.domain.use_cases

import com.beach_weather.base.core.domain.use_cases.UseCase.None

abstract class UseCase<out Type, in Params> where Type : Any {

    abstract fun run(params: Params): Type

    class None
}

fun none() = None()