package com.beach_weather.base.core.data.mappers

interface Mapper<T, R> where  T : Any?, R : Any? {
    fun map(input: T): R
}