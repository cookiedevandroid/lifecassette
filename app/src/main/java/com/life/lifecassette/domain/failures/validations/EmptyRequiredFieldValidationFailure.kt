package com.life.lifecassette.domain.failures.validations

import core.exception.Failure

class EmptyRequiredFieldValidationFailure() : Failure.ValidationFailure()