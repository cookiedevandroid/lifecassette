package network.core.experimental

import com.google.gson.JsonSyntaxException
import io.reactivex.*
import io.reactivex.functions.Function
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.io.IOException
import java.lang.reflect.Type

class RxErrorHandlingCallAdapterFactory : CallAdapter.Factory {

    private val original: RxJava2CallAdapterFactory

    companion object {
        fun createWithScheduler(scheduler: Scheduler): CallAdapter.Factory =
            RxErrorHandlingCallAdapterFactory(scheduler)

        @Suppress("unused")
        fun create(): CallAdapter.Factory =
            RxErrorHandlingCallAdapterFactory()
    }

    private constructor() {
        original = RxJava2CallAdapterFactory.create()
    }

    private constructor(scheduler: Scheduler) {
        original = RxJava2CallAdapterFactory.createWithScheduler(scheduler)
    }

    @Suppress("UNCHECKED_CAST")
    override fun get(
        returnType: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {
        (original.get(
            returnType,
            annotations,
            retrofit
        ) as? CallAdapter<Any, Any>)?.let { adapter ->
            return RxCallAdapterWrapper(
                retrofit,
                adapter
            )
        }
        return null
    }

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    private class RxCallAdapterWrapper<R>(
        private val retrofit: Retrofit,
        private val wrapped: CallAdapter<R, Any>
    ) : CallAdapter<R, Any> {
        override fun adapt(call: Call<R>?): Any {
            return when (val result = wrapped.adapt(call)) {
                is Single<*> -> return result.onErrorResumeNext {
                    Single.error(asRetrofitException(it))
                }
                is Completable -> result.onErrorResumeNext {
                    Completable.error(asRetrofitException(it))
                }
                is Observable<*> -> result.onErrorResumeNext(Function<Throwable, Observable<Nothing>> { t ->
                    Observable.error<Nothing>(asRetrofitException(t))
                })
                is Flowable<*> -> result.onErrorResumeNext(Function<Throwable, Flowable<Nothing>> { t ->
                    Flowable.error<Nothing>(asRetrofitException(t))
                })
                is Maybe<*> -> result.onErrorResumeNext(Function<Throwable, Maybe<Nothing>> { source ->
                    Maybe.error { asRetrofitException(source) }
                })
                else -> result
            }
        }

        override fun responseType(): Type = wrapped.responseType()

        @Throws(IOException::class)
        // TODO change to factory that will produce more suitable errors, like AuthException, NotFoundException, etc.
        // TODO this errors could contain localized error. This approach could remove some boilerplate of err handling, and remove ErrorHandler as entity from the projects at all !!! F*ck yes! =)
        private fun asRetrofitException(throwable: Throwable): RetrofitException {

            // We had non-200 http error
            if (throwable is HttpException) {
                val response = throwable.response()
                val url = response?.raw()?.request?.url.toString()
                return RetrofitException.httpError(url, response, retrofit)
            }

            if (throwable is JsonSyntaxException) {
                return RetrofitException.parseError(throwable)
            }
            // A network error happened
            return if (throwable is IOException) {
                RetrofitException.connectionError(throwable)
            } else RetrofitException.unexpectedError(
                throwable
            )

            // We don't know what happened. We need to simply convert to an unknown error
        }
    }
}