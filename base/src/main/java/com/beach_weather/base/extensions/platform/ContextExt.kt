@file:Suppress("unused")

package extensions.platform

import android.app.Activity
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Point
import android.location.LocationManager
import android.net.ConnectivityManager
import android.os.Build
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import java.lang.reflect.InvocationTargetException

fun Context.isLocationEnable(): Boolean =
    getLocationManager().isProviderEnabled(LocationManager.GPS_PROVIDER)

fun Context.getLocationManager() = getSystemService(Context.LOCATION_SERVICE) as LocationManager

fun Context.dpToPx(dp: Float): Float {
    val resources = resources
    val metrics = resources.displayMetrics
    val px = dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    return px
}

fun Context.convertDpToPx(dp: Float): Int {
    val resources = resources
    val metrics = resources.displayMetrics
    val px = dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    return px.toInt()
}

fun Context.pxToDp(px: Float): Float {
    val resources = resources
    val metrics = resources.displayMetrics
    val dp = px * DisplayMetrics.DENSITY_DEFAULT / (metrics.densityDpi.toFloat())
    return dp
}

fun Context.getColorCompat(@ColorRes colorId: Int): Int = ContextCompat.getColor(this, colorId)
//fun Fragment.getColorCompat(@ColorRes colorId: Int) = activity.getColorCompat(colorId)

fun Fragment.getColorCompat(@ColorRes colorId: Int) =
    ContextCompat.getColor(this.activity as Context, colorId)

fun Activity.hideKeyBoard() {
    val view = this.currentFocus
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view?.windowToken, 0)
    view?.clearFocus()
}

fun Activity.clearCurrentFocus() {
    currentFocus?.clearFocus()
}

fun Activity.showKeyBoard() {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}

fun Context.getColorFromAttrs(@AttrRes resId: Int): Int {
    val typedValue = TypedValue()
    val theme = this.theme
    theme.resolveAttribute(resId, typedValue, true)
    return typedValue.data
}

fun Context.toastSH(text: CharSequence) {
    Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
}

fun Context.toastLN(text: CharSequence) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show()
}

fun Context.toastLN(@StringRes resId: Int) {
    Toast.makeText(this, resId, Toast.LENGTH_LONG).show()
}

fun Context.getNavigationBarPosition(): Point {
    val appUsableSize = getAppUsableScreenSize()
    val realScreenSize = getRealScreenSize()

    // navigation bar on the right
    if (appUsableSize.x < realScreenSize.x) {
        return Point(realScreenSize.x - appUsableSize.x, appUsableSize.y);
    }

    // navigation bar at the bottom
    if (appUsableSize.y < realScreenSize.y) {
        return Point(appUsableSize.x, realScreenSize.y - appUsableSize.y);
    }

    // navigation bar is not present
    return Point()
}

fun Context.getNavigationBarHeight(): Int {
    val appUsableSize = getAppUsableScreenSize()
    val realScreenSize = getRealScreenSize()

    // navigation bar at the bottom
    if (appUsableSize.y < realScreenSize.y) {
        return (realScreenSize.y - appUsableSize.y)
    }

    // navigation bar on the right
    if (appUsableSize.x < realScreenSize.x) {
        return (realScreenSize.x - appUsableSize.x)
    }

    return 0
}

fun Context.getAppUsableScreenSize(): Point {
    val windowManager = this.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val display = windowManager.defaultDisplay
    val size = Point()
    display.getSize(size)
    return size
}

fun Context.toastNotImplemented() {
    Toast.makeText(this, """ not implemented yet ¯\_(ツ)_/¯ """, Toast.LENGTH_SHORT).show()
}

fun Context.getRealScreenSize(): Point {
    val windowManager = this.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val display = windowManager.defaultDisplay
    val size = Point()

    if (Build.VERSION.SDK_INT >= 17) {
        display.getRealSize(size)
    } else if (Build.VERSION.SDK_INT >= 14) {
        try {
            size.x = (Display::class.java.getMethod("getRawWidth").invoke(display) as Int)
            size.y = (Display::class.java.getMethod("getRawHeight").invoke(display) as Int)
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        } catch (e: NoSuchMethodException) {
            e.printStackTrace()
        }
    }

    return size
}

fun Context.inflate(
    @LayoutRes resId: Int,
    root: ViewGroup? = null,
    attachToRoot: Boolean = false
): View = LayoutInflater.from(this).inflate(resId, root, attachToRoot)

fun Context.getCompatColor(@ColorRes resId: Int) = ContextCompat.getColor(this, resId)
fun Context.getCompatDrawable(@DrawableRes resId: Int) = ContextCompat.getDrawable(this, resId)

@Deprecated("Change to new Api {@link android.net.ConnectivityManager.NetworkCallback}")
fun Context.isOnline(): Boolean {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val netInfo = cm.activeNetworkInfo
    return netInfo != null && netInfo.isConnectedOrConnecting
}

fun Context.dpToPx(dps: Int): Int {
    return Math.round(this.resources.displayMetrics.density * dps).toInt()
}

fun Context.getDrawableFromVector(res: Int): VectorDrawableCompat? {
    return VectorDrawableCompat.create(this.resources, res, this.theme)
}

fun Context.getBitmapFromVectorDrawableRes(res: Int): Bitmap? {
    val vectorDrawable = getDrawableFromVector(res)
    return vectorDrawable?.let {
        val bitmap =
            Bitmap.createBitmap(it.intrinsicWidth, it.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        it.setBounds(0, 0, canvas.width, canvas.height)
        it.draw(canvas)
        bitmap
    }
}

fun Context.withStyleAttributes(
    attrs: AttributeSet?,
    filter: IntArray,
    block: TypedArray.() -> Unit
) {
    with(obtainStyledAttributes(attrs, filter)) {
        block()
        recycle()
    }
}

fun Activity.getStatusBarHeight(): Int {
    val resId = resources.getIdentifier("status_bar_height", "dimen", "android")
    return if (resId > 0) resources.getDimensionPixelSize(resId)
    else (Math.ceil(if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) 25.0 else 24.0) * resources
        .displayMetrics
        .density
            ).toInt()
}

fun Context.pixelsToDp(pixels: Int): Float {
    val metrics = resources.displayMetrics
    return pixels.toFloat() / (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT)
}



