package core.platform

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment

/**
 * Created by artCore on 2/6/18.
 */
abstract class BaseCoreFragmentDialog : DialogFragment() {

    abstract val layoutRes: Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutRes, container)
    }

    // on MEIZU it looks ugly without this feature, have dark background on top of content =/
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val d = super.onCreateDialog(savedInstanceState)
        d.window?.requestFeature(Window.FEATURE_NO_TITLE)
        return d
    }

    protected fun adjustDialogSize() {
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    protected fun forceHeightWrapContent(v: View) {
        // Start with the provided view
        var current = v

        // Travel up the tree until fail, modifying the LayoutParams
        do {
            // Get the parent
            val parent = current.parent

            // Check if the parent exists
            if (parent != null) {
                // Get the view
                try {
                    current = parent as View
                } catch (e: ClassCastException) {
                    // This will happen when at the top view, it cannot be cast to a View
                    break
                }

                // Modify the layout
                current.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            }
        } while (current.parent != null)

        // Request a layout to be re-done
        current.requestLayout()
    }

    companion object {
        private val SCREEN_WIDTH_COEF = 2.6
    }
}