package utils

import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.View
import androidx.annotation.ColorInt

class ClickableSpannable(
    private val id: Int,
    @ColorInt val spanColor: Int,
    private val isUnerlined: Boolean = false,
    private val clickListener: Listener
) : ClickableSpan() {

    interface Listener {
        fun onClick(view: View, associatedId: Int)
    }

    private fun updateStateOfDrawing(ds: TextPaint) {
        val cFilter = PorterDuffColorFilter(
            spanColor,
            PorterDuff.Mode.SRC_ATOP
        )
        ds.colorFilter = cFilter
    }

    override fun onClick(textView: View) {
        clickListener.onClick(textView, id)
    }

    override fun updateDrawState(ds: TextPaint) {
        super.updateDrawState(ds)
        updateStateOfDrawing(ds)
        ds.isUnderlineText = isUnerlined
    }
}