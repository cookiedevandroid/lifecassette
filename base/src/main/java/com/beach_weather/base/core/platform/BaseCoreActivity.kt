package core.platform

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import io.reactivex.disposables.CompositeDisposable


abstract class BaseCoreActivity : AppCompatActivity() {
    protected val disposables: CompositeDisposable by lazy { CompositeDisposable() }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    protected fun <T> LiveData<T>.observe(action: (T) -> Unit) {
        this.observe(this@BaseCoreActivity, Observer {
            action(it)
        })
    }
}
