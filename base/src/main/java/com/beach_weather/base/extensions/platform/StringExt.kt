package extensions.platform

import android.graphics.Typeface.BOLD
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Base64
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.Collections.replaceAll
import java.util.regex.Pattern

/**
 * Created by artcore on 05.05.16.
 */
/**
 * there are " "(space) and "-" also included
 */
fun String.isAllLettersLatin(): Boolean = this.matches(Regex("[a-zA-Z-]+"))

fun String.isAllDigits(): Boolean = this.matches(Regex("[0-9]+"))

fun String.isAllLetters(): Boolean = this.toCharArray().all { it.isLetter() }

fun String.toBearer(): String = "Bearer $this"

/* fast decision, could made better, but had no time */
fun String.isDate(pattern: String): Boolean {
    val dateFormat = SimpleDateFormat(pattern, Locale.getDefault())
    dateFormat.isLenient = false
    try {
        dateFormat.parse(this.trim())
    } catch (pe: ParseException) {
        return false
    }

    return true
}

fun String.leaveNumbers(): Int = Regex("[^0-9]").replace(this, "").toInt()

fun String.replaceHtmlSymbols(): String? {
    return replace("&quot;", "\"")
        .replace("&amp;", "&")
        .replace("&apos;", "'")
}

fun String.noPlus() = replace("+", "")

fun String.getInnerPosition(targetText: String): Pair<Int, Int> {
    require(contains(targetText))
    val start = this.indexOf(targetText)
    return Pair(start, start + targetText.length)
}

fun String.find(pattern: String): String? {
    val intsOnly = Pattern.compile(pattern)
    val makeMatch = intsOnly.matcher(this)
    makeMatch.find()
    return try {
        makeMatch.group()
    } catch (ex: IllegalStateException) {
        null
    }
}

fun String?.doIfNotBlank(action: (String) -> Unit) {
    if (this?.isNotBlank() == true) action(this)
}

fun String.toDate(pattern: String): Date? {
    val format = SimpleDateFormat(pattern, Locale.getDefault())
    return format.parse(this)
}

fun String.decodeBase64(): String {
    val dataDec: ByteArray = Base64.decode(this, Base64.DEFAULT)
    var decodedString = ""
    try {
        decodedString = String(dataDec, Charset.forName("UTF-8"))
    } catch (e: UnsupportedEncodingException) {
        e.printStackTrace()
    } finally {
        return decodedString
    }
}

fun String.encodeBase64(): String {
    val dataDec: ByteArray = Base64.encode(this.toByteArray(), Base64.DEFAULT)
    var encodedString = ""
    try {
        encodedString = String(dataDec, Charset.forName("UTF-8"))
    } catch (e: UnsupportedEncodingException) {
        e.printStackTrace()
    } finally {
        return encodedString.replace("\n", "")
    }
}

fun SpannableString.spanBold(fullText: String, spannedText: String): SpannableString {
    val (start, end) = fullText.getInnerPosition(spannedText)
    this.setSpan(StyleSpan(BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    return this
}

fun SpannableString.spanColor(fullText: String, spannedText: String, color: Int): SpannableString {
    val (start, end) = fullText.getInnerPosition(spannedText)
    this.setSpan(ForegroundColorSpan(color), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    return this
}

