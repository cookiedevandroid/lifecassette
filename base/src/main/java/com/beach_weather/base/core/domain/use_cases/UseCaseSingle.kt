package com.beach_weather.base.core.domain.use_cases

import io.reactivex.Single

abstract class UseCaseSingle<Type, in Params> : UseCase<Single<Type>, Params>() where Type : Any? {

    abstract override fun run(params: Params): Single<Type>
}