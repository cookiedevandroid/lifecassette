package data.net.pagination

import com.google.gson.annotations.SerializedName

abstract class BasePagination {
    @field:SerializedName("limit")
    val limit: Int? = null

    @field:SerializedName("offset")
    val offset: Int? = null

    @field:SerializedName("total_count")
    val totalCount: Int? = null

    override fun toString(): String =
        "BasePagination(limit=$limit, offset=$offset, totalCount=$totalCount)"
}


