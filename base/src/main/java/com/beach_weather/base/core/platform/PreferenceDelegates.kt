package core.platform

import android.annotation.SuppressLint
import android.content.SharedPreferences
import kotlin.reflect.KProperty

/**
 * Created by artCore on 9/15/17.
 */
class PrefStringDelegate(private val preferences: SharedPreferences) {
    operator fun getValue(thisRef: Any, property: KProperty<*>): String {
        return preferences.getString(property.name, "") ?: ""
    }

    @SuppressLint("ApplySharedPref")
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
        preferences.edit().putString(property.name, value).commit()
    }
}

class PrefLongDelegate(private val preferences: SharedPreferences) {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): Long {
        return preferences.getLong(property.name, -1)
    }

    @SuppressLint("ApplySharedPref")
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Long) {
        preferences.edit().putLong(property.name, value).commit()
    }
}

class PrefIntDelegate(private val preferences: SharedPreferences, val defVal: Int) {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): Int {
        return preferences.getInt(property.name, defVal)
    }

    @SuppressLint("ApplySharedPref")
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Int) {
        preferences.edit().putInt(property.name, value).commit()
    }
}

class PrefBooleanDelegate(private val preferences: SharedPreferences) {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): Boolean {
        return preferences.getBoolean(property.name, false)
    }

    @SuppressLint("ApplySharedPref")
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Boolean) {
        preferences.edit().putBoolean(property.name, value).commit()
    }
}