package data.net.pagination

import com.google.gson.annotations.SerializedName

open class PaginationGenericResponse<out T>: BasePagination() {
  @SerializedName("list") private val _list: List<T> ? = emptyList()
  val list: List<T>
    get() = _list ?: emptyList()
}
