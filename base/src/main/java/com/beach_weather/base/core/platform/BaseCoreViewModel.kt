package core.platform

import androidx.lifecycle.ViewModel
import extensions.rx.plus
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.exceptions.UndeliverableException

/**
 * Created by artCore on 7/13/18.
 */
abstract class BaseCoreViewModel : ViewModel() {
    private val disposables: CompositeDisposable by lazy { CompositeDisposable() }

    override fun onCleared() {
        super.onCleared()
        try {
            disposables.clear()
        } catch (e: UndeliverableException) {
            e.printStackTrace()
        }
    }

    fun bindDisposable(disposable: Disposable) {
        disposables + disposable
    }

    fun autoDisposable(disposableFun: () -> Disposable) {
        disposables + disposableFun()
    }
}


