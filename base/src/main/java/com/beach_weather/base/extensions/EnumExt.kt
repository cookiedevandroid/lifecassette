package extensions

import java.util.*

inline fun <reified T : Enum<T>> enumValueOrDefault(name: String?, default: T): T {
    if (name == null) return default
    return try {
        enumValueOf(name.toUpperCase())
    } catch (ex: java.lang.IllegalArgumentException) {
        default
    }
}

inline fun <reified T : Enum<T>> enumValueIgnoreCase(name: String?): T? {
    if (name == null) return null
    return try {
        enumValueOf<T>(name.toUpperCase())
    } catch (ex: java.lang.IllegalArgumentException) {
        null
    }
}

@Suppress("unused")
inline fun <reified T : Enum<T>> enumValueOrNull(name: String?): T? {
    if (name == null) return null

    return try {
        enumValueOf<T>(name)
    } catch (ex: java.lang.IllegalArgumentException) {
        null
    }
}

inline fun <reified T : Enum<T>> deserialize(string: String): T? = enumValues<T>().find {
    it.name.equals(string, ignoreCase = true)
}

inline fun <reified T : Enum<T>> find(predicate: (T) -> Boolean): T? =
    enumValues<T>().find { predicate(it) }

fun <T : Enum<T>> Enum<T>.serialize(): String = this.name.toLowerCase(Locale.US)

