package com.life.lifecassette.domain.failures

import com.beach_weather.base.core.domain.failures.base.BaseThrowableToFailureConverter
import com.life.lifecassette.utils.extentions.isLogsEnabled
import core.exception.Failure

class ThrowableToFailureConverter : BaseThrowableToFailureConverter() {
    override fun map(input: Throwable?): Failure {
        if (isLogsEnabled()) input?.printStackTrace()
        return super.map(input)
    }
}