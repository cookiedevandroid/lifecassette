package data.net

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class NetErrorModel(
    @SerializedName("message") val msg: String,
    @SerializedName("code") val code: Int = -1
) : Serializable