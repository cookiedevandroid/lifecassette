package network.core.experimental

import com.google.gson.JsonSyntaxException
import data.net.NetErrorModel
import data.net.base.BaseResponse
import retrofit2.Response
import retrofit2.Retrofit
import java.io.EOFException
import java.io.IOException

class RetrofitException internal constructor(message: String?,
    /** The request URL which produced the error.  */
    val url: String?,
    /** Response object containing status code, headers, body, etc.  */
    val response: Response<*>?,
    /** The event kind which triggered this error.  */
    val kind: Kind,
    exception: Throwable?,
    /** The Retrofit this request was executed on  */
    val retrofit: Retrofit?,
    val netError: NetErrorModel? = null) : RuntimeException(message, exception) {


  /** Identifies the event kind which triggered a [RetrofitException].  */
  enum class Kind {
    /** An [IOException] occurred while communicating to the server.  */
    NETWORK,
    /** A non-200 HTTP status code was received from the server.  */
    HTTP,
    CONVERSION,
    /**
     * An internal error occurred while attempting to execute a request. It is best practice to
     * re-throw this exception so your application crashes.
     */
    UNEXPECTED
  }

  /**
   * HTTP response body converted to specified `type`. `null` if there is no
   * response.
   *
   * @throws IOException if unable to convert the body to the specified `type`.
   */
  @Throws(IOException::class)
  @Deprecated("Don't use this", replaceWith = ReplaceWith("convertError()"))
  private fun <T> getErrorBodyAs(type: Class<T>): T? {
    if (response?.errorBody() == null) {
      return null
    }
    val converter = retrofit?.responseBodyConverter<T>(type, arrayOfNulls(0))
    return try {
      converter?.convert(response.errorBody()!!)
    } catch (ex: Exception) {
      ex.printStackTrace()
      when (ex) {
        !is EOFException, !is JsonSyntaxException -> ex.printStackTrace()
        else -> throw ex
      }
      null
    }
  }

  override fun toString(): String {
    return "RetrofitException(url=$url, response=$response, kind=$kind, retrofit=$retrofit, netError=$netError)"
  }

  companion object {
    private fun <T> convertError(retrofit: Retrofit, type: Class<T>, response: Response<*>?): T? {
      if (response?.errorBody() == null) {
        return null
      }
      else {
        val converter = retrofit.responseBodyConverter<T>(type, arrayOfNulls(0))
        return try {
          converter?.convert(response.errorBody())
        } catch (ex: Exception) {
          ex.printStackTrace()
          when (ex) {
            !is EOFException, !is JsonSyntaxException -> ex.printStackTrace()
            else -> throw ex
          }
          null
        }
      }
    }

    @Throws(IOException::class)
    fun httpError(url: String?, response: Response<*>?, retrofit: Retrofit): RetrofitException {
      val message: String = if (response != null) {
        "${response.code()}" + " " + response.message()
      } else
        "unknown error"

      val netError = convertError(retrofit, BaseResponse::class.java, response)?.error
      return RetrofitException(message, url, response, Kind.HTTP, null, retrofit, netError)
    }

    fun connectionError(exception: IOException): RetrofitException =
        RetrofitException(exception.message, null, null, Kind.NETWORK, exception, null)

    fun parseError(exception: Throwable): RetrofitException =
        RetrofitException(exception.message, null, null, Kind.CONVERSION, exception, null)

    fun unexpectedError(exception: Throwable): RetrofitException =
        RetrofitException(exception.message, null, null, Kind.UNEXPECTED, exception, null)
  }
}

