package com.life.lifecassette.utils.extentions

import com.google.gson.Gson
import com.google.gson.JsonIOException
import com.google.gson.JsonSyntaxException
import java.io.Serializable

@Throws(JsonIOException::class) fun Serializable.toJson(gson: Gson): String {
    return gson.toJson(this)
}

@Throws(JsonSyntaxException::class) fun <T> String.to(gson: Gson, type: Class<T>): T where T : Serializable {
    return gson.fromJson(this, type)
}